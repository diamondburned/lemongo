package siji

const (
	// ChatBubble glyph
	ChatBubble = ''

	// SolidChatBubble glyph
	SolidChatBubble = ''

	// Speaker glyph
	Speaker = ''

	// MusicNote glyph
	MusicNote = ''

	// Play icon
	Play = ''

	// Pause icon
	Pause = ''

	// Previous icon
	Previous = ''

	// Next icon
	Next = ''

	// Urgent glyph (!)
	Urgent = ''

	// Exchange glyph
	Exchange = ''

	// Clock glyph
	Clock = ''
)
