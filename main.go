package main

import (
	"time"

	"gitlab.com/diamondburned/lemongo/lemonbar"
	"gitlab.com/diamondburned/lemongo/notify"
	"gitlab.com/diamondburned/lemongo/widgets"
	"gitlab.com/diamondburned/lemongo/widgets/clock"
	"gitlab.com/diamondburned/lemongo/widgets/mpris"
)

var (
	foregroundColor = 0xFAFAFA
	secondaryColor  = 0xAAAAAA
	accentColor     = 0x4285F4
	blurColor       = 0x666666
	backgroundColor = 0x1A1A1A
)

func main() {
	bar, err := lemonbar.NewBar(&lemonbar.Options{
		Bottom:  true,
		Monitor: "f", // First monitor
		Fonts: []string{
			"Tewi", "Siji",
		},
		Clickable:  10,
		Foreground: int64(foregroundColor),
		Background: int64(backgroundColor),
	})

	if err != nil {
		panic(err)
	}

	n, err := notify.Listen()
	if err != nil {
		panic(err)
	}

	ws := widgets.NewWidgets()

	m, err := mpris.NewWidget(ws)
	if err != nil {
		panic(err)
	}

	m.TrimTitle = 10

	ws.MiddleWidgets = []widgets.Widget{m}

	ws.RightMargin = 2
	ws.RightWidgets = []widgets.Widget{
		clock.NewClock(ws, "3:04:05 PM", foregroundColor, accentColor),
	}

	for {
		select {
		case notification := <-n:
		Main:
			for {
				if notification == nil {
					break Main
				}

				notify := &Notify{
					Notification: notification,
					ValueScale:   0.25,
				}

				bar.Draw(notify.Draw())

				var d = 5 * time.Second
				if notify.Timeout > 0 {
					d = notify.Timeout
				} else if notify.Value != nil {
					d = 1 * time.Second
				}

				select {
				case <-time.After(d):
					break Main
				case notification = <-n:
				}
			}

		case <-ws.Event:
			bar.Draw(ws.Draw())
		}
	}
}
