module gitlab.com/diamondburned/lemongo

require (
	github.com/BurntSushi/xgb v0.0.0-20160522181843-27f122750802 // indirect
	github.com/BurntSushi/xgbutil v0.0.0-20160919175755-f7c97cef3b4e
	github.com/davecgh/go-spew v1.1.1
	github.com/godbus/dbus v4.1.0+incompatible
	github.com/valyala/fasttemplate v1.0.1
	gitlab.com/diamondburned/clocker v0.0.0-20190409150028-8b1f16fd9670
)
