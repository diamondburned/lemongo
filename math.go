package main

func min(i, j int) int {
	if i < 0 {
		return 0
	}

	if i < j {
		return i
	}

	return j
}
