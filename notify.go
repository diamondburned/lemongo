package main

import (
	"fmt"
	"math"
	"strings"

	"gitlab.com/diamondburned/lemongo/notify"
	"gitlab.com/diamondburned/lemongo/siji"
)

// Notify implements the progress view
type Notify struct {
	*notify.Notification
	ValueScale float64
}

// Draw draws the notification
func (n *Notify) Draw() string {
	var s strings.Builder

	if n.Value != nil {
		if strings.Contains(n.Application, "volume") {
			s.WriteRune(siji.Speaker)
		} else {
			s.WriteRune(siji.Exchange)
		}

		if n.ValueScale == 0 {
			n.ValueScale = 1
		}

		var (
			max    = int(100 * n.ValueScale)
			value  = math.Ceil(float64(*n.Value) * n.ValueScale)
			active = min(int(value), max)
			blur   = max - active
		)

		fmt.Fprintf(&s,
			" %%{F#%X}%s%%{F#%X}%s%%{F-}",
			foregroundColor,
			strings.Repeat(progressDash, active),
			blurColor,
			strings.Repeat(progressDash, blur),
		)
	} else {
		fmt.Fprintf(&s, "%%{B#%X} ", accentColor)

		switch n.Urgency {
		case notify.UrgencyMax, notify.UrgencyCritical:
			s.WriteRune(siji.Urgent)
		default:
			s.WriteRune(siji.ChatBubble)
		}

		fmt.Fprintf(&s,
			" %s %%{B-} %%{F#%X}%s %%{F#%X}%s%%{F-}",
			n.Application, foregroundColor, n.Title, secondaryColor, n.Body,
		)
	}

	return "%{c}" + s.String()
}
