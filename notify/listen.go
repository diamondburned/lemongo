// Package notify interfaces with dbus to listen to all
// incoming notifications. The package only supports one
// notification at a time, and would discard the last one
// if it receives a new one.
package notify

import "time"

const (
	appName    = "lemongo"
	appVendor  = "diamondburned"
	appVersion = "git"
)

var (
	nch = make(chan *Notification, 10)
)

// Notification is the struct of a notification
type Notification struct {
	Application string

	Title string
	Body  string
	Icon  string

	// Defaults to UrgencyNormal
	Urgency Urgency

	// Value shows progress
	Value *int32

	Timeout time.Duration
}

// Listen returns the channel which would be filled with
// either the notification itself or nil if it wants to close
// the old notification.
func Listen() (chan *Notification, error) {
	err := initialize()
	return nch, err
}
