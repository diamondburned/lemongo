package notify

// Urgency is the constant for the urgency of a notification
type Urgency uint8

const (
	UrgencyMinimum Urgency = iota
	UrgencyLow
	UrgencyNormal
	UrgencyCritical
	UrgencyMax
)
