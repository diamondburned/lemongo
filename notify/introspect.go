package notify

const dbusIntrospect = `
<node name="` + dbusPath + `">
	<interface name="` + dbusName + `">
		<method name="Notify">
			<arg direction="in"  name="app_name"        type="s"/>
			<arg direction="in"  name="replaces_id"     type="u"/>
			<arg direction="in"  name="app_icon"        type="s"/>
			<arg direction="in"  name="summary"         type="s"/>
			<arg direction="in"  name="body"            type="s"/>
			<arg direction="in"  name="actions"         type="as"/>
			<arg direction="in"  name="hints"           type="a{sv}"/>
			<arg direction="in"  name="expire_timeout"  type="i"/>
			<arg direction="out" name="id"              type="u"/>
		</method>
		<method name="GetServerInformation">
			<arg direction="out" name="name"            type="s"/>
			<arg direction="out" name="vendor"          type="s"/>
			<arg direction="out" name="version"         type="s"/>
			<arg direction="out" name="spec_version"    type="s"/>
		</method>
		<method name="CloseNotification">
			<arg direction="in"  name="id"              type="u"/>
		</method>
	</interface>
</node>`
