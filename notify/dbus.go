package notify

import (
	"errors"

	"github.com/godbus/dbus"
	"github.com/godbus/dbus/introspect"
)

const (
	dbusPath = `/org/freedesktop/Notifications`
	dbusName = `org.freedesktop.Notifications`
)

var (
	d *dbus.Conn

	errNameTaken = errors.New("Name already taken")
)

type state struct{}

func initialize() (err error) {
	if d != nil {
		return nil
	}

	d, err = dbus.SessionBusPrivate()
	if err != nil {
		return err
	}

	if err := d.Auth(nil); err != nil {
		d.Close()
		return err
	}

	if err := d.Hello(); err != nil {
		d.Close()
		return err
	}

	reply, err := d.RequestName(dbusName, dbus.NameFlagDoNotQueue)
	if err != nil {
		return err
	}

	if reply != dbus.RequestNameReplyPrimaryOwner {
		return errNameTaken
	}

	if err := d.Export(notify{}, dbusPath, dbusName); err != nil {
		return err
	}

	if err := d.Export(
		introspect.Introspectable(dbusIntrospect),
		dbusPath, "org.freedesktop.DBus.Introspectable",
	); err != nil {
		return err
	}

	return nil
}
