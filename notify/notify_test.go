package notify

import (
	"fmt"
	"testing"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/godbus/dbus"
)

func TestNotify(t *testing.T) {
	const (
		title = "Test"
		body  = "Moo."
	)

	phaser("Start a listening goroutine")

	nch, err := Listen()
	if err != nil {
		t.Fatal(err)
	}

	go func() {
		for {
			select {
			case n := <-nch:
				spew.Dump(n)

				if n.Title != title {
					t.Fatal("Invalid title")
				}

				if n.Body != body {
					t.Fatal("Invalid body")
				}

				if n.Timeout != (time.Second * 5) {
					t.Fatal("Invalid timeout")
				}
			case <-time.After(2 * time.Second):
				t.Fatal("Test timed out")
			}
		}
	}()

	phaser("Send a test notification")

	conn, err := dbus.SessionBus()
	if err != nil {
		t.Fatal(err)
	}

	obj := conn.Object("org.freedesktop.Notifications", "/org/freedesktop/Notifications")

	call := obj.Call(
		"org.freedesktop.Notifications.Notify", 0, "", uint32(0),
		"", title, body, []string{},
		map[string]dbus.Variant{}, int32(5000),
	)

	if call.Err != nil {
		t.Fatal(call.Err)
	}
}

var phase int

func phaser(s string) {
	phase++
	fmt.Printf("Phase %d: %s\n", phase, s)
}
