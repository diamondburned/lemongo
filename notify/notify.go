package notify

import (
	"time"

	"github.com/godbus/dbus"
)

type notify struct{}

func (notify) CloseNotification(id uint32) *dbus.Error {
	if nch != nil {
		nch <- nil
	}

	return nil
}

func (notify) Notify(
	appName string, replacesID uint32, appIcon string,
	title string, body string, actions []string,
	hints map[string]dbus.Variant, expireTimeout int32) (uint32, *dbus.Error) {

	n := &Notification{
		Application: appName,
		Title:       title,
		Body:        body,
		Icon:        appIcon,
		Urgency:     UrgencyNormal,
		Timeout:     time.Duration(expireTimeout) * time.Millisecond,
	}

	if v, ok := hints["urgency"]; ok {
		if u, ok := v.Value().(uint8); ok {
			n.Urgency = Urgency(u)
		}
	}

	if v, ok := hints["value"]; ok {
		if i, ok := v.Value().(int32); ok {
			n.Value = &i
		}
	}

	nch <- n

	return 0, nil
}

func (n notify) GetServerInformation() (
	name string, vendor string, version string,
	specVersion string, err *dbus.Error) {

	name = appName
	vendor = appVendor
	version = appVersion
	specVersion = appVersion

	return
}
