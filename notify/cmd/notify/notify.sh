#!/usr/bin/env bash
# This script provides a simple function to demonstrate
# how to use this Go binary.
#
# To build the binary, simply run `go build`
# To run this script, do `./notify | ./notify.sh`

# Set up the delimiter, do not change this
d=$(printf "\u0004")

while read -d "$d" code; do
	# Evaluate the output, which should be sh-safe.
	# An associate array is made after eval-ing.
	eval "$code"

	echo "Received notification titled ${notify["title"]}"

	for i in "${!notify[@]}"; {
		echo -e "\${notify[\"$i\"]} - ${notify[$i]:-nil}"
	}
done

