package main

import (
	"fmt"
	"strconv"

	"github.com/valyala/fasttemplate"
	"gitlab.com/diamondburned/lemongo/notify"
)

const template = `
declare -A notify
notify["application"]=$'(app)'
notify["title"]=$'(title)'
notify["body"]=$'(body)'
notify["icon"]=$'(icon)'
notify["urgency"]=$'(urgency)'
notify["value"]=$'(value)'
notify["timeout"]=$'(timeout)'
` + "\u0004"

func main() {
	t := fasttemplate.New(template, "(", ")")

	n, err := notify.Listen()
	if err != nil {
		panic(err)
	}

	for n := range n {
		m := map[string]interface{}{
			"app":     n.Application,
			"title":   n.Title,
			"body":    n.Body,
			"icon":    n.Icon,
			"urgency": reflectUrgency(n.Urgency),
			"value":   "",
			"timeout": strconv.FormatFloat(
				n.Timeout.Seconds(),
				'f', -1, 64,
			),
		}

		if n.Value != nil {
			m["value"] = strconv.Itoa(int(*n.Value))
		}

		fmt.Print(t.ExecuteString(m))
	}
}

func reflectUrgency(u notify.Urgency) string {
	switch u {
	case notify.UrgencyMinimum:
		return "minimum"
	case notify.UrgencyLow:
		return "low"
	case notify.UrgencyNormal:
		return "normal"
	case notify.UrgencyCritical:
		return "critical"
	case notify.UrgencyMax:
		return "max"
	default:
		return ""
	}
}
