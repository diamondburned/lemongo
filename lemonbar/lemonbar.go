package lemonbar

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

// Bar contains structs for a bar
type Bar struct {
	args []string
	opts *Options

	cmd *exec.Cmd
	sh  *exec.Cmd

	input io.WriteCloser

	draw      chan struct{}
	forcedraw chan string
	state     string

	d time.Duration
}

// Options is the options for a Lemonbar.
// If Bottom is true, Geometry should be empty.
type Options struct {
	Geometry string
	Monitor  string // f/l/[0-9]
	Bottom   bool
	Fonts    []string

	Clickable int

	Foreground int64
	Background int64
}

// NewBar makes a new bar
func NewBar(opts *Options) (*Bar, error) {
	bar := &Bar{
		opts:      opts,
		draw:      make(chan struct{}),
		forcedraw: make(chan string),
	}

	var args []string

	if opts.Geometry != "" {
		args = append(args, "-g", opts.Geometry)
	}

	if opts.Bottom {
		args = append(args, "-b")
	}

	if opts.Foreground > 0 {
		args = append(args, "-F", "#"+strconv.FormatInt(opts.Foreground, 16))
	}

	if opts.Background > 0 {
		args = append(args, "-B", "#"+strconv.FormatInt(opts.Background, 16))
	}

	if opts.Clickable > 0 {
		args = append(args, "-a", strconv.Itoa(opts.Clickable))
	}

	for _, f := range opts.Fonts {
		args = append(args, "-f", f)
	}

	println(strings.Join(args, " "))

	bar.cmd = exec.Command("lemonbar", args...)

	stdin, err := bar.cmd.StdinPipe()
	if err != nil {
		return nil, err
	}

	bar.input = stdin

	bar.sh = exec.Command("sh")
	bar.sh.Stdout = os.Stdout
	bar.sh.Stderr = os.Stderr

	sh, err := bar.sh.StdinPipe()
	if err != nil {
		return nil, err
	}

	bar.cmd.Stdout = sh

	if err := bar.sh.Start(); err != nil {
		return nil, err
	}

	if err := bar.cmd.Start(); err != nil {
		return nil, err
	}

	go bar.loop()

	return bar, nil
}

func (b *Bar) loop() {
	for {
		fmt.Fprintln(b.input)

		select {
		case <-b.draw:
			fmt.Fprint(b.input, "%{S"+b.opts.Monitor+"}"+b.state)
		}
	}
}

// Draw draws onto the bar
func (b *Bar) Draw(s string) {
	// Set the state cache to that
	b.state = s
	b.draw <- struct{}{}
}

// Stop closes stdin
func (b *Bar) Stop() {
	b.input.Close()
	close(b.draw)
}
