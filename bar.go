package main

const (
	progressDash = "―"
)

// Bar implements method Draw() to draw the bar
type Bar interface {
	Draw() string
}
