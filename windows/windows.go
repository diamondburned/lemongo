package windows

import (
	"time"

	"github.com/BurntSushi/xgbutil"
	"github.com/BurntSushi/xgbutil/ewmh"
)

// Window is the struct for one window
type Window struct {
	ID    uint32
	Title string
}

// Listen polls X for window changes
func Listen(done chan struct{}) (<-chan *Window, error) {
	ch := make(chan *Window)

	conn, err := xgbutil.NewConn()
	if err != nil {
		return nil, err
	}

	go func() {
		t := time.NewTicker(50 * time.Millisecond)

		var old string

		for {
			select {
			case <-done:
				return
			case <-t.C:
				w, err := ewmh.ActiveWindowGet(conn)
				if err != nil {
					continue
				}

				name, err := ewmh.WmNameGet(conn, w)
				if err != nil {
					continue
				}

				if old == name {
					continue
				}

				old = name

				ch <- &Window{
					ID:    uint32(w),
					Title: name,
				}
			}
		}
	}()

	return ch, nil
}
