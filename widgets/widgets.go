package widgets

import (
	"io"
	"strings"
)

var (
	// PaddingRune is the rune used for padding
	PaddingRune = ' '

	// MarginRune is the rune used for margins
	MarginRune = ' '
)

// Widgets holds all the widgets
type Widgets struct {
	LeftMargin  int
	LeftPadding int
	LeftWidgets []Widget

	RightMargin  int
	RightPadding int
	RightWidgets []Widget

	MiddlePadding int
	MiddleWidgets []Widget

	Event chan struct{}

	done chan struct{}
	dest io.Writer
}

// NewWidgets takes in a writer to draw the widgets on
func NewWidgets() *Widgets {
	draw := make(chan struct{}, 8)
	ws := &Widgets{
		Event: draw,
	}

	return ws
}

// Die kills
func (ws *Widgets) Die() {
	close(ws.Event)
}

// Render calls the bar to render. It does not block if the bar fails to draw.
func (ws *Widgets) Render() {
	select {
	case ws.Event <- struct{}{}:
	default:
	}
}

// Draw draws all the widgets
func (ws *Widgets) Draw() string {
	s := &strings.Builder{}

	// Align to the left of the screen and reset the colors
	s.WriteString("%{F-}%{B-}%{l}")

	// Draw the left margin
	RepeatRune(s, MarginRune, ws.LeftMargin)

	// Draw the widgets
	for i, w := range ws.LeftWidgets {
		s.WriteString(w.Draw())

		// Render paddings, except after the last widget
		if i != len(ws.LeftWidgets)-1 {
			RepeatRune(s, PaddingRune, ws.LeftPadding)
		}
	}

	// Align to center
	s.WriteString("%{F-}%{B-}%{c}")

	for i, w := range ws.MiddleWidgets {
		s.WriteString(w.Draw())

		// Render paddings, except after the last widget
		if i != len(ws.MiddleWidgets)-1 {
			RepeatRune(s, PaddingRune, ws.MiddlePadding)
		}
	}

	s.WriteString("%{F-}%{B-}%{r}")

	// Draw the widgets
	for i, w := range ws.RightWidgets {
		s.WriteString(w.Draw())

		// Render paddings, except after the last widget
		if i != len(ws.RightWidgets)-1 {
			RepeatRune(s, PaddingRune, ws.RightPadding)
		}
	}

	// Draw the right margin
	RepeatRune(s, MarginRune, ws.RightMargin)

	return s.String()
}

// RepeatRune repeats a rune
func RepeatRune(s *strings.Builder, r rune, c int) {
	for i := 0; i < c; i++ {
		s.WriteRune(r)
	}
}
