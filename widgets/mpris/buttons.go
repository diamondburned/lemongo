package mpris

import "gitlab.com/diamondburned/lemongo/siji"

const (
	// Play plays the song
	Play = "%{A:playerctl pause:} " + string(siji.Pause) + " %{A}"

	// Pause pauses the song
	Pause = "%{A:playerctl play:} " + string(siji.Play) + " %{A}"

	// Prev moves previously
	Prev = "%{A:playerctl previous:} " + string(siji.Previous) + " %{A}"

	// Next skips the song
	Next = "%{A:playerctl next:} " + string(siji.Next) + " %{A}"
)
