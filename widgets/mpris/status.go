package mpris

import "github.com/godbus/dbus"

// Status shows playback status
type Status string

const (
	Playing Status = "Playing"
	Paused  Status = "Paused"
)

func (s *Status) parseStatus(v dbus.Variant) error {
	playback, ok := v.Value().(string)
	if !ok {
		return errInvalidVariant
	}

	*s = Status(playback)
	return nil
}
