package main

import (
	"encoding/json"
	"fmt"

	"github.com/davecgh/go-spew/spew"
	"github.com/godbus/dbus"
	"gitlab.com/diamondburned/lemongo/widgets/mpris"
)

func main() {
	c, err := mpris.InitDbus()
	if err != nil {
		panic(err)
	}

	p := mpris.NewPlayer()

	for v := range c {
		spew.Dump(v)
		if len(v.Body) < 2 {
			continue
		}

		variant := dbus.MakeVariant(v.Body[1])
		p.ParseMPRISVariant(variant)

		if j, err := json.Marshal(p); err == nil {
			fmt.Print("\n" + string(j))
		}
	}
}
