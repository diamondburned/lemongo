package mpris

import "github.com/godbus/dbus"

const (
	dbusPath      = "/org/mpris/MediaPlayer2"
	dbusInterface = "org.freedesktop.DBus.Properties"
)

// InitDbus starts a private Dbus connection
func InitDbus() (chan *dbus.Message, error) {
	conn, err := dbus.SessionBusPrivate()
	if err != nil {
		return nil, err
	}

	if err := conn.Auth(nil); err != nil {
		conn.Close()
		return nil, err
	}

	if err := conn.Hello(); err != nil {
		conn.Close()
		return nil, err
	}

	call := conn.BusObject().Call(
		"org.freedesktop.DBus.AddMatch", 0,
		"type='signal',path='"+dbusPath+"',interface='"+dbusInterface+"'",
	)

	if call.Err != nil {
		return nil, call.Err
	}

	c := make(chan *dbus.Message, 10)
	conn.Eavesdrop(c)

	return c, nil
}
