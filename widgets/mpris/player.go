package mpris

import (
	"errors"

	"github.com/godbus/dbus"
)

var (
	errInvalidVariant   = errors.New("Invalid variant")
	errMetadataNotFound = errors.New("No metadata found")
	errStatusNotFound   = errors.New("No playback status found")
)

// Player contains the struct for a player
type Player struct {
	Status   Status
	Metadata *Metadata
}

// NewPlayer returns a new player
func NewPlayer() *Player {
	return &Player{
		Metadata: &Metadata{},
	}
}

// ParseMPRISVariant parses for possible variant info
func (p *Player) ParseMPRISVariant(v dbus.Variant) error {
	variants, ok := v.Value().(map[string]dbus.Variant)
	if !ok {
		return errInvalidVariant
	}

	pbVariant, ok := variants["PlaybackStatus"]
	if !ok {
		return errStatusNotFound
	}

	if err := p.Status.parseStatus(pbVariant); err != nil {
		return err
	}

	mdVariant, ok := variants["Metadata"]
	if !ok {
		return errMetadataNotFound
	}

	if err := p.Metadata.parseMetadata(mdVariant); err != nil {
		return err
	}

	return nil
}
