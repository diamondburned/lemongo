package mpris

import (
	"errors"
	"fmt"
	"strings"
	"testing"

	"github.com/godbus/dbus"
)

func testGetPlayer(conn *dbus.Conn) (string, error) {
	var dbusList []string
	err := conn.BusObject().Call("org.freedesktop.DBus.ListNames", 0).Store(&dbusList)
	if err != nil {
		return "", err
	}

	for _, v := range dbusList {
		if strings.HasPrefix(v, "org.mpris.MediaPlayer2") {
			return v, nil
		}
	}

	return "", errors.New("No MPRIS supporting players found")
}

func TestStatus(t *testing.T) {
	conn, err := dbus.SessionBus()
	if err != nil {
		t.Fatal(err)
	}

	playername, err := testGetPlayer(conn)
	if err != nil {
		t.Fatal(err)
	}

	mpris := conn.Object(playername, "/org/mpris/MediaPlayer2")
	status, err := mpris.GetProperty("org.mpris.MediaPlayer2.Player.PlaybackStatus")
	if err != nil {
		t.Fatal(err)
	}

	s, err := parseStatus(status)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(s)
}

func TestMetadata(t *testing.T) {
	conn, err := dbus.SessionBus()
	if err != nil {
		t.Fatal(err)
	}

	playername, err := testGetPlayer(conn)
	if err != nil {
		t.Fatal(err)
	}

	mpris := conn.Object(playername, "/org/mpris/MediaPlayer2")
	metadata, err := mpris.GetProperty("org.mpris.MediaPlayer2.Player.Metadata")
	if err != nil {
		t.Fatal(err)
	}

	m, err := parseMetadata(metadata)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(m.Format(10))
}
