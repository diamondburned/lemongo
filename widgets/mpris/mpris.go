package mpris

import (
	"github.com/davecgh/go-spew/spew"
	"github.com/godbus/dbus"
	"gitlab.com/diamondburned/lemongo/widgets"
)

// Widget implements the mpris widget
type Widget struct {
	TrimTitle int

	state string
	last  *Player
	done  chan struct{}
}

// NewWidget makes a new clock with the precision of a second
func NewWidget(ws *widgets.Widgets) (*Widget, error) {
	c, err := InitDbus()
	if err != nil {
		return nil, err
	}

	w := &Widget{
		done: make(chan struct{}),
	}

	p := NewPlayer()

	go func() {
		for {
			select {
			case v := <-c:
				spew.Dump(v)

				if len(v.Body) < 2 {
					w.state = ""
					continue
				}

				variant := dbus.MakeVariant(v.Body[1])
				p.ParseMPRISVariant(variant)

				var status string
				switch p.Status {
				case Playing:
					status = Play
				default:
					status = Pause
				}

				w.state = Prev + status + Next + " " +
					p.Metadata.Format(w.TrimTitle)

				ws.Render()
			case <-w.done:
				return
			}
		}
	}()

	return w, nil
}

// Draw draws the clock
func (w *Widget) Draw() string {
	return w.state
}
