package mpris

// PlayerColors contains all known players and their colors
var PlayerColors = map[string]int{
	"":          0x0, // default color
	"spotify":   0x1DB954,
	"rhythmbox": 0xD8BB1F,
}
