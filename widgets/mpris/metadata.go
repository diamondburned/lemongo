package mpris

import (
	"strings"

	"github.com/godbus/dbus"
)

// Metadata contains player metadata
type Metadata struct {
	MprisTrackid     string
	MprisLength      uint64
	MprisArtURL      string
	XesamAlbum       string
	XesamAlbumArtist []string
	XesamArtist      []string
	XesamAutoRating  float64
	XesamDiscNumber  int32
	XesamTitle       string
	XesamTrackNumber int32
	XesamURL         string
	XesamGenre       []string
	XesamUserRating  float32
}

func (m *Metadata) parseMetadata(v dbus.Variant) error {
	values, ok := v.Value().(map[string]dbus.Variant)
	if !ok {
		return errInvalidVariant
	}

	for k, variant := range values {
		v := variant.Value()
		switch v := v.(type) {
		case string:
			switch k {
			case "mpris:artUrl":
				m.MprisArtURL = v
			case "xesam:album":
				m.XesamAlbum = v
			case "xesam:title":
				m.XesamTitle = v
			case "xesam:url":
				m.XesamURL = v
			}
		case []string:
			switch k {
			case "xesam:albumArtist":
				m.XesamAlbumArtist = make([]string, len(v))
				for i, v := range v {
					m.XesamAlbumArtist[i] = v
				}
			case "xesam:artist":
				m.XesamArtist = make([]string, len(v))
				for i, s := range v {
					m.XesamArtist[i] = s
				}
			}
		case int32:
			switch k {
			case "xesam:discNumber":
				m.XesamDiscNumber = v
			case "xesam:trackNumber":
				m.XesamTrackNumber = v
			}
		case uint64:
			if k == "mpris:length" {
				m.MprisLength = v
			}
		case float64:
			if k == "xesam:autoRating" {
				m.XesamAutoRating = v
			}
		case dbus.ObjectPath:
			m.MprisTrackid = string(v)
		}
	}

	return nil
}

// Format prints the string
func (m *Metadata) Format(l int) string {
	s := strings.Builder{}

	var (
		artist = trimRunes(humanize(m.XesamArtist), l)
		title  = trimRunes(m.XesamTitle, l*2+3-len([]rune(artist)))
	)

	s.WriteString(artist)
	s.WriteString(" - ")
	s.WriteString(title)

	tl := (int(l)*2 + 3) - s.Len()
	if tl > 0 {
		return strings.Repeat(" ", tl) + s.String()
	}

	return s.String()
}

func humanize(s []string) string {
	switch len(s) {
	case 0:
		return ""
	case 1:
		return s[0]
	case 2:
		return s[0] + " and " + s[1]
	default:
		return strings.Join(s[:len(s)-2], ", ") + " and " + s[len(s)-1]
	}
}

func trimRunes(s string, l int) string {
	if l < 1 {
		return s
	}

	r := []rune(s)
	if len(r) > l {
		return string(r[:l-1]) + "…"
	}

	return s
}

func max(i, j int) int {
	if i > j {
		return i
	}

	return j
}
