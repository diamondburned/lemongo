package widgets

// Widget is the interface for widgets
type Widget interface {
	Draw() string
}
