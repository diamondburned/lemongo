package clock

import (
	"fmt"
	"time"

	"gitlab.com/diamondburned/clocker"
	"gitlab.com/diamondburned/lemongo/siji"
	"gitlab.com/diamondburned/lemongo/widgets"
)

type Clock struct {
	state string
}

// NewClock makes a new clock with the precision of a second
func NewClock(ws *widgets.Widgets, format string, fg, bg int) *Clock {
	c := &Clock{}

	go func() {
		for t := range clocker.NewTicker(time.Second).C {
			c.state = fmt.Sprintf(
				"%%{B#%X}%%{F#%X} %s "+string(siji.Clock)+" %%{B-}%%{F-}",
				bg, fg, t.Format(format),
			)

			ws.Render()
		}
	}()

	return c
}

// Draw draws the clock
func (c *Clock) Draw() string {
	return c.state
}
